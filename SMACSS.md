BASE
    Los aplicados a elementos HTML (no clases, no ID)
    Casi casi como un normalize
    Inicializar tus estilos

TEMA
    Look and feel de la web o aplicación.

ESTADOS
    Cambios de estado .is-hide/.is-show

LAYOUT
    Maquetacion, sin estilos de tema

MÓDULOS
    Los componentes que se repiten a lo largo de la aplicacion.
    botones, iconos, widgets
